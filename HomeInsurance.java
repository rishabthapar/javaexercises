package Account;

public class HomeInsurance implements Detailable {
    private double amountInsured;
    private double premium;
    private double excess;

    public HomeInsurance(double premium, double excess, double amountInsured){
        this.premium = premium;
        this.amountInsured = amountInsured;
        this.excess = excess;
    }

    public String getDetails(){
        return  "" + premium + " " + excess + " " + amountInsured;
    }
    
}