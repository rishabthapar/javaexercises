package Account;

public class TestStrings {
    public static boolean isPallindrome(String s){
        int len = s.length();
        double half = Math.floor(len/2);
        for(int i =0; i<half;i++){
            if(s.charAt(i) != s.charAt(len-i-1)){
                return false;
            }
        }
        return true;
    }
    public static void main(String[] args) {
        String s1 = "example.doc";
        String s2 = "bak";
        String replaced = s1.substring(0,7) + s2;
        int compare = s1.compareTo(s2);
        if(compare==0){
            System.out.println("Equal!");
        }
        else if(compare < 0){
            System.out.println(s2);
        }
        else{
            System.out.println(s1);
        }
        String find = "the quick brown fox swallowed down the lazy chicken";
        int count =0;
        for(int i =0; i< find.length()-1;i++){
            if(find.charAt(i)=='o' && find.charAt(i+1) == 'w'){
                count++;
            }
        }
        System.out.println("Count =" + count);
        String check = "Live not on evil";
        if(isPallindrome(check)){
            System.out.println("Pallindrome!");

        }
        else{
            System.out.println("Not Pallindrome!");
        }
    }
    
}