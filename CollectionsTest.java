package Account;

import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;

public class CollectionsTest {

    public static void main(final String[] args) throws DodgyNameException {
        HashSet<Account> accounts;
        accounts = new HashSet<Account>();
        final SavingsAccount savingsAccount = new SavingsAccount("RST", 100.0);
        final CurrentAccount currentAccount = new CurrentAccount("ABC", 200.0);
        final SavingsAccount savingsAccount2 = new SavingsAccount("XYZ", 50.0);
        accounts.add(savingsAccount);
        accounts.add(currentAccount);
        accounts.add(savingsAccount2);
        final Iterator it = accounts.iterator();
        while (it.hasNext()) {
            final Account acc = (Account) it.next();
            System.out.println("Name: " + acc.getName());
            System.out.println("Balance before interest: " + acc.getBalance());
            acc.addInterest();
            System.out.println("Balance after interest: " + acc.getBalance());
        }

        for (final Account a : accounts) {
            System.out.println("Name: " + a.getName());
            System.out.println("Balance before interest: " + a.getBalance());
            a.addInterest();
            System.out.println("Balance after interest: " + a.getBalance());
        }

        accounts.forEach(acct -> {
            System.out.println("Name: " + acct.getName());
            System.out.println("Balance before interest: " + acct.getBalance());
            acct.addInterest();
            System.out.println("Balance after interest: " + acct.getBalance());
        });

        TreeSet<Account> treeAccounts;
        treeAccounts = new TreeSet<Account>(new AccountComparator());
        treeAccounts.add(savingsAccount);
        treeAccounts.add(savingsAccount2);
        treeAccounts.add(currentAccount);
        System.out.println("************TREEE STRUCTURE**************");
        for (final Account a : treeAccounts) {
            System.out.println("Name: " + a.getName());
            System.out.println("Balance: " + a.getBalance());
        }
    }  
}