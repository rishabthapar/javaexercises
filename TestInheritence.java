package Account;
public class TestInheritence {
    public static void main(String[] args) throws DodgyNameException {
        Account[] accounts = new Account[3];
        accounts[0] = new SavingsAccount("A",2.0);
        accounts[1] = new SavingsAccount("B", 4.0);
        accounts[2] = new CurrentAccount("C", 6.0);
        for(int i =0; i< accounts.length; i++){
            System.out.println("Account name: " + accounts[i].getName());
            System.out.println("Account balance before interest: " + accounts[i].getBalance());
            accounts[i].addInterest();
            System.out.println("Account balance after interest: " + accounts[i].getBalance());
        }
    }
    
}