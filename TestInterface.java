package Account;

public class TestInterface {
    public static void main(String[] args) throws DodgyNameException {
        Detailable[] detailables = new Detailable[3];
        detailables[0] = new CurrentAccount("ABC", 100.0);
        detailables[1] = new SavingsAccount("RST", 200.0);
        detailables[2] = new HomeInsurance(20.0,300.0,100.0);
        for(int i = 0; i<detailables.length;i++){
            System.out.println(detailables[i].getDetails());
        }
    }
    
}