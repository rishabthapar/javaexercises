package Account;

public abstract class Account implements Detailable {
    private String name;
    private double balance;
    private static double interestRate = 1.1;

    public Account(String name, double balance) throws DodgyNameException{
        if (name == "Fingers"){
            throw new DodgyNameException();
        }
        else{
            this.name = name;
        }
        this.balance= balance;
    }

    public Account() throws DodgyNameException{
        this.name = "Rishab";
        this.balance = 50.0;
        if (name == "Fingers"){
            throw new DodgyNameException();
        }
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name) throws DodgyNameException{
        if (name == "Fingers"){
            throw new DodgyNameException();
        }
        else{
            this.name = name;
        }
    }

    public Double getBalance(){
        return this.balance;
    }

    public void setBalance(double balance){
        this.balance = balance;
    }

    public static void setInterestRate(double ir){
        interestRate = ir;
    }

    public static double getInterestRate(){
        return interestRate;
    }

    public abstract void addInterest();

    public boolean withdraw(double amt){
        if(this.balance > amt){
            this.balance -= amt;
            return true;
        }
        else{
            return false;
        }
    }

    public boolean withdraw(){
        return withdraw(20.0);
    }

    public String getDetails(){
        return "Name: " +getName() + " Balance: " + getBalance();
    }

}