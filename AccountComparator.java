package Account;

import java.util.Comparator;

public class AccountComparator implements Comparator<Account> {

    @Override
    public int compare(Account a1, Account a2) {
        double diff = a1.getBalance() - a2.getBalance();
        return (int) diff;
    }


    
}