package Account;

public class TestAccount2 {
    public static void main(String[] args) {
        Account[] arrayOfAccounts = new Account[5];
        String[] names = {"Picard", "Ryker", "Worf", "Troy", "Fingers"};
        double[] amounts = {23,5444,2,345,34};
        for(int i =0 ; i<arrayOfAccounts.length; i++){
            try {
                arrayOfAccounts[i] = new SavingsAccount(names[i], amounts[i]);
            } catch (Exception DodgyNameException) {
                System.out.println("Invalid name "+ DodgyNameException.toString());
            }
            
            System.out.println(arrayOfAccounts[i].getName());
            System.out.println(arrayOfAccounts[i].getBalance());
            arrayOfAccounts[i].addInterest();
            System.out.println("Balance after interest: "+ arrayOfAccounts[i].getBalance());
            if(arrayOfAccounts[i].withdraw()){
                System.out.println("Withdrawal Succesful, new balance: "+ arrayOfAccounts[i].getBalance());
            }
            else{
                System.out.println("Withdrawal Failed, not enough funds");
            }
        }
    }
    
}